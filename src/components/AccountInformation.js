import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm} from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class AccountInformation extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            accountType: 1
        };
    }

    handleUserInput(e, index, valueAccount) {
        if(valueAccount){
            this.setState({accountType: valueAccount});
        } else {
            const name = e.target.name;
            const value = e.target.value;
            this.setState({[name]: value});
        }
    }

    handleChange(event, index, value) {
        this.handleUserInput(event, index, value)
    }

    render() {
        return (<div>
            <ValidatorForm
                ref="form"
                onSubmit={this.props.handleNext}
                onError={errors => console.log(errors)}>
                <SelectField
                    floatingLabelText="Account Type"
                    name="accountType"
                    value={this.state.accountType}
                    onChange={this.handleChange}
                >
                    <MenuItem value={1} primaryText="Savings" />
                    <MenuItem value={2} primaryText="Deposit" />
                </SelectField>
                <br />
                <TextField
                    floatingLabelText="Purpose of bank account"
                    multiLine={true}
                />
                <br />
                <TextField
                    floatingLabelText="Where will the money come from"
                    multiLine={true}
                />
                <br />
                <TextField
                    floatingLabelText="How did you hear about us?"
                    multiLine={true}
                />
                <div style={{paddingTop: 40}}>
                    <FlatButton
                        label="Back"
                        disabled={this.props.stepIndex === 0}
                        onTouchTap={this.props.handlePrev}
                        style={{marginRight: 12}}
                    />
                    <RaisedButton
                        label={this.props.stepIndex === 4 ? 'Finish' : 'Next'}
                        primary={true}
                        type="submit"
                    />
                </div>
            </ValidatorForm>
        </div>
        )
    }
}
export default AccountInformation;