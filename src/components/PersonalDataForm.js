import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';

class PersonalDataForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            email: '',
            homePhone: '',
            mobile: '',
            firstName: '',
            socialSecurity: '',
            lastName: ''
        };
    }

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    }

    handleChange(event) {
        this.handleUserInput(event)
    }

    render() {
        return (<div>
            <ValidatorForm
                ref="form"
                onSubmit={this.props.handleNext}
                onError={errors => console.log(errors)}>
                <TextValidator
                    floatingLabelText="First Name"
                    onChange={this.handleChange}
                    name="firstName"
                    value={this.state.firstName}
                    validators={['required']}
                    errorMessages={['this field is required']}
                />
                <br />
                <TextField
                    floatingLabelText="Middle Name"
                />
                <br />
                <TextValidator
                    floatingLabelText="Last Name"
                    onChange={this.handleChange}
                    name="lastName"
                    value={this.state.lastName}
                    validators={['required']}
                    errorMessages={['this field is required']}
                />
                <br />
                <TextValidator
                    floatingLabelText="Home Phone"
                    validators={['isNumber']}
                    onChange={this.handleChange}
                    name="homePhone"
                    value={this.state.homePhone}
                    errorMessages={['number should contain digits only']}
                />
                <br />
                <TextValidator
                    floatingLabelText="Mobile Phone"
                    validators={['isNumber']}
                    onChange={this.handleChange}
                    name="mobile"
                    value={this.state.mobile}
                    errorMessages={['number should contain digits only']}
                    errorText=""
                />
                <br />
                <TextValidator
                    floatingLabelText="Email Address"
                    onChange={this.handleChange}
                    name="email"
                    value={this.state.email}
                    validators={['required', 'isEmail']}
                    errorMessages={['this field is required', 'email is not valid']}
                />
                <br />
                <TextField
                    floatingLabelText="Mailing Address"
                />
                <br />
                <TextValidator
                    floatingLabelText="Social Security Number"
                    name="socialSecurity"
                    value={this.state.socialSecurity}
                    validators={['required', 'isNumber']}
                    errorMessages={['this field is required', 'should contains digits only']}
                    onChange={this.handleChange}
                />
                <div style={{paddingTop: 40}}>
                    <FlatButton
                        label="Back"
                        disabled={this.props.stepIndex === 0}
                        onTouchTap={this.props.handlePrev}
                        style={{marginRight: 12}}
                    />
                    <RaisedButton
                        label={this.props.stepIndex === 4 ? 'Finish' : 'Next'}
                        primary={true}
                        type="submit"
                    />
                </div>
            </ValidatorForm>
        </div>
        )
    }
}
export default PersonalDataForm;