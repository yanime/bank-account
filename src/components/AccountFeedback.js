import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

class AccountFeedback extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            feedbackMessage: null
        }
    }

    setMessage(message) {
        this.setState({feedbackMessage: message});
    }

    componentDidMount(){
        if(Math.floor(Math.random()*2)){
            fetch('http://fiveloop.snco.co/api/success.json').then(function(response) {
                return response.json();
            }).then(function(json) {
                this.setMessage('Your Bank Account Number is : ' + json.data.id)
            }.bind(this))
        } else {
            //using the same api, error api returns 400 unauthorized
            fetch('http://fiveloop.snco.co/api/success.json').then(function(response) {
                return response.json();
            }).then(function(json) {
                this.setMessage('The application was rejected please call at +34 555 666 222 with application code : ' + json.data.id)
            }.bind(this));
        }
    }

    render() {
        return (<div>
            {this.state.feedbackMessage ? (
            <p>{this.state.feedbackMessage}</p>
            ) : (
                <CircularProgress size={80} thickness={5} />
            )}
        </div>
        )
    }
}
export default AccountFeedback;