import React from 'react';
import {
    Step,
    Stepper,
    StepLabel,
    } from 'material-ui/Stepper';
import InitialStep from './InitialStep';
import PersonalDataForm from './PersonalDataForm'
import EmploymentInformation from './EmploymentInformation'
import AccountInformation from './AccountInformation'
import AccountFeedback from './AccountFeedback'

class HorizontalLinearStepper extends React.Component {

    state = {
        finished: false,
        stepIndex: 0
    };

    handleNext = () => {
        const {stepIndex} = this.state;
        this.setState({
            stepIndex: stepIndex + 1,
            finished: stepIndex >= 4
        });
    };

    handlePrev = () => {
        const {stepIndex} = this.state;
        if (stepIndex > 0) {
            this.setState({stepIndex: stepIndex - 1});
        }
    };

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return <InitialStep
                    stepIndex={this.state.stepIndex}
                    handleNext={this.handleNext}
                    handlePrev={this.handlePrev}>
                </InitialStep>;
            case 1:
                return <PersonalDataForm
                    stepIndex={this.state.stepIndex}
                    handleNext={this.handleNext}
                    handlePrev={this.handlePrev}>
                </PersonalDataForm>;
            case 2:
                return <EmploymentInformation
                    stepIndex={this.state.stepIndex}
                    handleNext={this.handleNext}
                    handlePrev={this.handlePrev}>
                </EmploymentInformation>;
            case 3:
                return <AccountInformation
                stepIndex={this.state.stepIndex}
                handleNext={this.handleNext}
                handlePrev={this.handlePrev}>
            </AccountInformation>;
            case 4:
                return <AccountFeedback></AccountFeedback>;
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    render() {
        const {stepIndex} = this.state;
        const contentStyle = {margin: '0 16px'};

        return (
            <div style={{width: '100%', maxWidth: 700, margin: 'auto'}} className="stepper-wrapper">
                <Stepper activeStep={stepIndex}>
                    <Step>
                        <StepLabel>Init</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Personal Information</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Employment Information</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Account Information</StepLabel>
                    </Step>
                    <Step>
                        <StepLabel>Documentation</StepLabel>
                    </Step>
                </Stepper>
                <div style={contentStyle}>
                  <div>
                      <div>{this.getStepContent(stepIndex)}</div>
                  </div>
                </div>
            </div>
        );
    }
}

export default HorizontalLinearStepper;