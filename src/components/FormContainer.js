import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import HorizontalLinearStepper from './HorizontalLinearStepper';

class FormContainer extends React.Component {
    render() {
        return (
            <div className="form-container">
                <MuiThemeProvider>
                    <HorizontalLinearStepper/>
                </MuiThemeProvider>
            </div>
        );
    }
}

export default FormContainer;