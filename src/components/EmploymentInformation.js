import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import FlatButton from 'material-ui/FlatButton';

class EmploymentInformation extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            employerName: '',
            workPhone: ''
        };
    }

    handleUserInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value});
    }

    handleChange(event) {
        this.handleUserInput(event)
    }

    render() {
        return (<div>
            <ValidatorForm
                ref="form"
                onSubmit={this.props.handleNext}
                onError={errors => console.log(errors)}>
                <TextValidator
                    floatingLabelText="Employer Name"
                    name="employerName"
                    value={this.state.employerName}
                    validators={['required']}
                    errorMessages={['this field is required']}
                    onChange={this.handleChange}
                />
                <br />
                <TextField
                    floatingLabelText="Employer Address"
                />
                <br />
                <TextValidator
                    floatingLabelText="Work Phone"
                    name="workPhone"
                    value={this.state.workPhone}
                    validators={['required', 'isNumber']}
                    errorMessages={['this field is required', 'should contains digits only']}
                    onChange={this.handleChange}
                />
                <br />
                <TextField
                    floatingLabelText="Job Position"
                />
                <div style={{paddingTop: 40}}>
                    <FlatButton
                        label="Back"
                        disabled={this.props.stepIndex === 0}
                        onTouchTap={this.props.handlePrev}
                        style={{marginRight: 12}}
                    />
                    <RaisedButton
                        label={this.props.stepIndex === 4 ? 'Finish' : 'Next'}
                        primary={true}
                        type="submit"
                    />
                </div>
            </ValidatorForm>
        </div>
        )
    }
}
export default EmploymentInformation;