import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Checkbox from 'material-ui/Checkbox';
import FlatButton from 'material-ui/FlatButton';

class InitialStep extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            checked: false
        };
    }

    onAcceptTermCond = (e, value) => {
        this.setState({checked: value});
    };

    render() {
        const contentStyle = {display: 'flex'};
        return (<div style={contentStyle}>
            <span>I agree to the Terms and Conditions</span>
            <div style={{width: '10px'}}>
                <MuiThemeProvider>
                    <Checkbox
                        checked={this.state.checked}
                        onCheck={this.onAcceptTermCond}
                    />
                </MuiThemeProvider>
            </div>
            <div style={{paddingTop: 40}}>
                <FlatButton
                    label="Back"
                    disabled={this.props.stepIndex === 0}
                    onTouchTap={this.props.handlePrev}
                    style={{marginRight: 12}}
                />
                <RaisedButton
                    label={this.props.stepIndex === 4 ? 'Finish' : 'Next'}
                    disabled={!this.state.checked}
                    primary={true}
                    onTouchTap={this.props.handleNext}
                />
            </div>
        </div>
        )
    }
}

export default InitialStep;