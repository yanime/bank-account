import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import FormContainer from './components/FormContainer';

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>Welcome to YoYo Bank</h2>
                </div>
                <FormContainer></FormContainer>
            </div>
        );
    }
}

export default App;